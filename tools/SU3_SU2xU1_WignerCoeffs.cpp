#include <iomanip>
#include <iostream>

#include <su3.h>

// test whether eps LM belongs to (lm mu) irrep
bool belong(int lm, int mu, int eps, int LM) {
   double p = ((2 * lm - 2 * mu - eps) / 3.0 + LM) / 2.0;
   double q = ((2 * lm + 4 * mu - eps) / 3.0 - LM) / 2.0;

   // true if p is integer
   if (std::floor(p) != p)
   {
      return false;
   }
   // true if q is integer
   if (std::floor(q) != q) {
      return false;
   }

   // p and q are integers
   // check their ranges
   if (p < 0 || p > lm) {
      return false;
   }
   if (q < 0 || q > mu) {
      return false;
   }

   return true;
}

int main(int argc, char** argv) {
   su3::init(250);
   std::cout << std::setprecision(8);

   int lm1, mu1, lm2, mu2, lm3, mu3;
   int eps3sel, LM3sel;
   std::cout << "SU(3) Wigner coefficients in canonical basis: <(lm1 mu1) e1 LM1; (lm2 mu3) e2 "
                "LM2|| (lm3 mu3) e3 LM3>rho."
             << std::endl;

   std::cout << "Enter (lm1 mu1) " << std::endl;
   std::cin >> lm1 >> mu1;
   std::cout << "Enter (lm2 mu2) " << std::endl;
   std::cin >> lm2 >> mu2;
   std::cout << "Enter (lm3 mu3) " << std::endl;
   std::cin >> lm3 >> mu3;

   int rhomax = su3::mult(lm1, mu1, lm2, mu2, lm3, mu3);
   if (!rhomax) {
      std::cerr << "(" << lm1 << " " << mu1 << ") and (" << lm2 << " " << mu2
                << ") does not couple to (" << lm3 << " " << mu3 << ")" << std::endl;
      return EXIT_FAILURE;
   }

   int LM3, eps3;
   std::cout << "Enter 2*LM3 ... -1 to include all possible eps3 LM3 values:" << std::endl;
   std::cin >> LM3;
   if (LM3 == -1) {
      std::cout << "<(" << lm1 << " " << mu1 << ") e1 2*LM1; (" << lm2 << " " << mu2
                << ") e2 2*LM2 || (" << lm3 << " " << mu3 << ") e3 2*LM3> rho = 1 ... " << rhomax
                << std::endl;
      for (int p3 = 0; p3 <= lm3; ++p3) {
         for (int q3 = 0; q3 <= mu3; ++q3) {
            eps3 = 2 * lm3 + mu3 - 3 * (p3 + q3);
            LM3 = mu3 + p3 - q3;
            std::vector<std::tuple<int, int, int>> labels;
            std::vector<double> cgs;
            su3::xwu3(lm1, mu1, lm2, mu2, lm3, mu3, eps3, LM3, rhomax, labels, cgs);
            size_t index = 0;
            for (auto eps2_LM2_LM1 : labels) {
               int eps2, LM2, LM1;
               std::tie(eps2, LM2, LM1) = eps2_LM2_LM1;
               int eps1 = eps3 - eps2;
               std::cout << std::fixed << std::setw(8) << eps1 << "  " << LM1;
               std::cout << std::fixed << std::setw(13) << eps2 << "  " << LM2;
               std::cout << std::fixed << std::setw(15) << eps3 << "  " << LM3 << "  ";
               for (int rho = 0; rho < rhomax; ++rho) {
                  std::cout << std::fixed << std::setw(15) << std::setfill(' ') << cgs[index++];
               }
               std::cout << std::endl;
            }
         }
      }
   } else {
      std::cout << "Enter eps3:" << std::endl;
      std::cin >> eps3;

      if (!belong(lm3, mu3, eps3, LM3)) {
         std::cerr << "eps=" << eps3 << " and 2*LM=" << LM3 << " do not belong to (" << lm3 << " "
                   << mu3 << ") irrep." << std::endl;
         return EXIT_FAILURE;
      }
      std::cout << "<(" << lm1 << " " << mu1 << ") e1 2*LM1; (" << lm2 << " " << mu2
                << ") e2 2*LM2 || (" << lm3 << " " << mu3 << ") e3 2*LM3> rho = 1 ... " << rhomax
                << std::endl;
      std::vector<std::tuple<int, int, int>> labels;
      std::vector<double> cgs;
      su3::xwu3(lm1, mu1, lm2, mu2, lm3, mu3, eps3, LM3, rhomax, labels, cgs);
      size_t index = 0;
      for (auto eps2_LM2_LM1 : labels) {
         int eps2, LM2, LM1;
         std::tie(eps2, LM2, LM1) = eps2_LM2_LM1;
         int eps1 = eps3 - eps2;
         std::cout << std::fixed << std::setw(8) << eps1 << "  " << LM1;
         std::cout << std::fixed << std::setw(13) << eps2 << "  " << LM2;
         std::cout << std::fixed << std::setw(15) << eps3 << "  " << LM3 << "  ";
         for (int rho = 0; rho < rhomax; ++rho) {
            std::cout << std::fixed << std::setw(15) << std::setfill(' ') << cgs[index++];
         }
         std::cout << std::endl;
      }
   }
   su3::finalize();
   return EXIT_SUCCESS;
}
