###############################################################################
# Defintions of FetchContent and function to fetch if package not found.
#
# Anna E. McCoy
# Institute for Nuclear Theory 
#
# SPDX-License-Identifier: MIT
#
# 3/29/22 (aem) : Created
###############################################################################
cmake_minimum_required(VERSION 3.20)

include(FetchContent)
FetchContent_Declare(
  wigxjpf
  GIT_REPOSITORY git@github.com:nd-nuclear-theory/wigxjpf.git
  # Working commit as of (3/29/22) 15a6330dfe48c4fc85d6592fb892ef1468617fa5
  GIT_TAG        main
  GIT_SHALLOW    TRUE
)