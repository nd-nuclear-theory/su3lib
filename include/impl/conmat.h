#ifndef SU3_CONMAT_H
#define SU3_CONMAT_H

namespace su3
{

void Conmat(int I, int J, int lm, int mu, int kmax, int L, double* O);

} // namespace su3

#endif
