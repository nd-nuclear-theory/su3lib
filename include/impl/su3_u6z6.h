#ifndef SU3_SU3_U6Z6_H
#define SU3_SU3_U6Z6_H

#include <su3.h>

#include <vector>

namespace su3
{

// Compute Racah U6 recoupling coefficients
void wru3(int lm1, int mu1, int lm2, int mu2, int lm, int mu, int lm3, int mu3,
                         int lm12, int mu12, int lm23, int mu23, int rho12max, int rho12_3max,
                         int rho23max, int rho1_23max, std::vector<double>& dru3);

// Compute Racah Z6 recoupling coefficients
void wzu3(int lm1, int mu1, int lm2, int mu2, int lm, int mu, int lm3, int mu3,
                         int lm12, int mu12, int lm23, int mu23, int rho12max, int rho12_3max,
                         int rho23max, int rho1_23max, std::vector<double>& dru3);


// Racah W-coefficient
double drr3(int jj1, int jj2, int ll2, int ll1, int jj3, int ll3);
void dlut(int ma, int na, std::vector<int>& ia, std::vector<double>& da, int md);
void dbsr(int ma, std::vector<double>& da, std::vector<double>& db, std::vector<double>& dc,
          int md);

// T datatype to be used for computation of (lm2 mu2) x (lm3 mu3) --> (lm23 mu23) CGs 
// Here, for large values of rho12max, higher precision is needed for accurate 
// computation of SU(3)>SU(2)xU(1) CGs.
template <typename T>
void wru3_internal(int lm1, int mu1, int lm2, int mu2, int lm, int mu, int lm3, int mu3, int lm12, int mu12,
          int lm23, int mu23, int rho12max, int rho12_3max, int rho23max, int rho1_23max,
          std::vector<double>& dru3) {
   dru3.assign(rho12max * rho12_3max * rho23max * rho1_23max, 0.0);

   int J1TD, J1T, J2TD, J2T;
   int I1, I2, I3, I4, I5, I6, IAQ, IBQ, ICQ, IE12, IE23;
   int INDA, INDB, INDC, INDMAX;
   int IS, J12T, J12TD, J23T, J23TS, J2S, J2SB;
   int J3S, J3SB, J3SQ, J3T, J3TD, JJ12T, JJ12TA;
   int JJ12TB, JTDMAX, JTDMIN;
   int IE3MAX, IES, IESJ3S, IESMAX;
   int NA, NAB, NABC, NNCD, IDQ, JD, KD, JDKD, KDID;
   int KA, KABC, KABCD, KAIA;
   int KB, KBCDQ, KBCQ, KBIB, KBQ, KC, KCDQ, KCIC;
   int KCQ, KDQ, NECA, NECB, NECC, NECD;

   int X13 = lm23 + mu23 + 1;
   std::vector<int> IA(X13, 0);

   int n2 = (lm3 + mu3 + 1);
   std::vector<int> INDMAT, J2SMAX, J2TMAX;
   std::vector<int> J3SMAX, J3TMAX;

   std::vector<int> JXTA, JYTA, IEA;
   std::vector<int> JXTB, JYTB, IEB;
   std::vector<int> JXTC, JYTC, IEC;
   std::vector<int> JXTD, JYTD, IED;

   int X14 = X13 * rho1_23max;
   std::vector<double> DA(X14, 0.0);

   std::vector<double> DB(rho1_23max, 0), DT(rho1_23max, 0);

   double D1, D2, DC;

   NA = rho12max;
   NAB = NA * rho12_3max;
   NABC = NAB * rho23max;
   std::vector<double> DEWU3D;
   xewu3(lm1, mu1, lm23, mu23, lm, mu, 1, NECD, rho1_23max, JXTD, JYTD, IED, DEWU3D);
   INDMAX = JXTD.size();
   NNCD = NECD + 1;
   IDQ = (INDMAX - NNCD - 1) * rho1_23max;
   for (JD = 1; JD <= NNCD; JD++) {
      IDQ = IDQ + rho1_23max;
      IA[JD - 1] = JD;
      KDQ = -X13;
      for (KD = 1; KD <= rho1_23max; ++KD) {
         KDQ = KDQ + X13;
         JDKD = JD + KDQ;
         KDID = KD + IDQ;
         DA[JDKD - 1] = DEWU3D[KDID - 1];
      }
   }

   dlut(NNCD, rho1_23max, IA, DA, X13);

   // for large value of rho23max, we need to use higher precision to
   // compute SU(3)>SU(2)xU(1) coefficients
   // (lm2 mu2) x (lm3 mu3) -> rho23max(lm23 mu23)
   std::vector<T> DEWU3C;
   xewu3_internal(lm2, mu2, lm3, mu3, lm23, mu23, 1, NECC, rho23max, JXTC, JYTC, IEC, DEWU3C);
   std::vector<double> DEWU3B;
   xewu3(lm12, mu12, lm3, mu3, lm, mu, 1, NECB, rho12_3max, JXTB, JYTB, IEB, DEWU3B);
   std::vector<double> DEWU3A;
   xewu3(lm12, mu12, mu2, lm2, lm1, mu1, 1, NECA, rho12max, JXTA, JYTA, IEA, DEWU3A);

   I1 = lm + 2 * mu;
   I2 = lm12 + 2 * mu12;
   I3 = 4 * lm12 + 2 * mu12;
   I4 = 2 * I2;
   I5 = 2 * (lm12 - mu12);
   JTDMIN = std::max(0, std::max(NECA - lm2 - mu2, NECB - lm3 - mu3));
   JTDMAX = std::min(NECA, std::min(NECB, lm12 + mu12));
   D1 = (double)((lm1 + 1) * su3::dim(lm12, mu12)) / (double)(su3::dim(lm1, mu1));
   IE23 = lm1 + 2 * mu1 - I1;
   J23TS = lm23 + NECD + 2;
   KDQ = -NABC;
   // for large value of rho23max, we need to use higher precision to
   // compute SU(3)>SU(2)xU(1) coefficients accurately
   std::vector<T> DWU3;
   for (KD = 0; KD < rho1_23max; ++KD) {
      KDQ = KDQ + NABC;
      J23T = J23TS - 2 * IA[KD];
      D2 = std::sqrt(D1 * (J23T + 1));
   // for large value of rho23max, we need to use higher precision to
   // compute SU(3)>SU(2)xU(1) coefficients accurately
      xwu3_internal(lm2, mu2, lm3, mu3, lm23, mu23, IE23, J23T, DEWU3C, rho23max, INDMAX, J2SMAX,
                    J2TMAX, J3SMAX, J3TMAX, IE3MAX, INDMAT, DWU3);
      IESMAX = J3TMAX.size();
      IE12 = 3 * IESMAX - IE3MAX - I1;
      J12TD = (IE12 + I2) / 3;
      for (IES = 1; IES <= IESMAX; ++IES) {
         IE12 = IE12 - 3;
         J12TD = J12TD - 1;
         if (J12TD < JTDMIN || J12TD > JTDMAX) {
            continue;
         }
         JJ12TA = I3 + IE12;
         IS = I4 - IE12;
         if (IS < JJ12TA) {
            JJ12TA = IS;
         }
         JJ12TA = JJ12TA / 3 + 1;
         IS = (I5 - IE12) / 3;
         JJ12TB = JJ12TA - std::abs(IS);
         I6 = 2 * lm1 + IS;
         J2TD = NECA - J12TD;
         J3TD = NECB - J12TD;
         J3T = J3TMAX[IES - 1] + 2;
         J3SB = J3SMAX[IES - 1];
         J3SQ = -n2;
         for (J3S = 1; J3S <= J3SB; ++J3S) {
            J3SQ = J3SQ + n2;
            IESJ3S = IES + J3SQ;
            J3T = J3T - 2;
            J2T = J2TMAX[IESJ3S - 1] + 2;
            INDC = (INDMAT[IESJ3S - 1] - J2T) / 2;
            J2SB = J2SMAX[IESJ3S - 1];
            for (J2S = 1; J2S <= J2SB; ++J2S) {
               J2T = J2T - 2;
               ICQ = INDC * rho23max;
               INDC = INDC + 1;
               for (JJ12T = 1; JJ12T <= JJ12TB; JJ12T += 2) {
                  J12T = JJ12TA - JJ12T;

                  INDA = INDEX(J12TD, lm12, J12T, J2TD, mu2, J2T);
                  if (JXTA[INDA] < 0) {
                     continue;
                  }

                  INDB = INDEX(J12TD, lm12, J12T, J3TD, lm3, J3T);
                  if (JXTB[INDB] < 0) {
                     continue;
                  }
                  DC = D2 * drr3(lm1, J2T, lm, J3T, J12T, J23T);
                  IS = J12T + I6;
                  if (IS % 4) {
                     DC = -DC;
                  }
                  IAQ = INDA * rho12max;
                  IBQ = INDB * rho12_3max;
                  KCQ = -NAB;
                  for (KC = 0; KC < rho23max; ++KC) {
                     KCQ = KCQ + NAB;
                     KCDQ = KCQ + KDQ;
                     KCIC = KC + ICQ;
                     KBQ = -NA;
                     for (KB = 0; KB < rho12_3max; ++KB) {
                        KBQ = KBQ + NA;
                        KBCDQ = KBQ + KCDQ;
                        KBIB = KB + IBQ;
                        for (KA = 0; KA < rho12max; ++KA) {
                           KABCD = KA + KBCDQ;
                           KAIA = KA + IAQ;
                           dru3[KABCD] += DC * DEWU3A[KAIA] * DEWU3B[KBIB] * (double)DWU3[KCIC];
                        }
                     }
                  }
               }
            }
         }
      }
   }

   KCQ = -NAB;
   for (KC = 0; KC < rho23max; ++KC) {
      KCQ = KCQ + NAB;
      KBQ = -NA;
      for (KB = 0; KB < rho12_3max; ++KB) {
         KBQ = KBQ + NA;
         KBCQ = KBQ + KCQ;
         for (KA = 0; KA < rho12max; ++KA) {
            KABC = KA + KBCQ;
            KDQ = -NABC;
            for (KD = 0; KD < rho1_23max; ++KD) {
               KDQ = KDQ + NABC;
               KABCD = KABC + KDQ;
               DB[KD] = dru3[KABCD];
            }
            if (rho1_23max > 1) {
               dbsr(rho1_23max, DA, DB, DT, X13);
            } else {
               DB[0] = DB[0] / DA[0];
            }
            KDQ = -NABC;
            for (KD = 0; KD < rho1_23max; ++KD) {
               KDQ = KDQ + NABC;
               KABCD = KABC + KDQ;
               dru3[KABCD] = DB[KD];
            }
         }
      }
   }
}

// T datatype to be used for computation of (lm2 mu2) x (lm1 mu1) --> (lm12 mu12) CGs 
// Here, for large values of rho12max, higher precision is needed for accurate 
// computation of SU(3)>SU(2)xU(1) CGs.
template <typename T>
void wzu3_internal(int lm1, int mu1, int lm2, int mu2, int lm, int mu, int lm3, int mu3, int lm12, int mu12,
          int lm23, int mu23, int rho12max, int rho12_3max, int rho23max, int rho1_23max,
          std::vector<double>& dzu3) {
   dzu3.assign(rho12max * rho12_3max * rho23max * rho1_23max, 0.0);

   int J1TD, J1T, J2TD, J2T;
   int I1, I2, I3, I4, I5, I6, I7, IAQ, IBQ, ICQ, IE12, IE23;
   int IE1, IE3, J1TS, J3TDMA, JJ3TA, JJ3TB, IE1MAX, IE1TES, J1S, J1SQ, IESJ1S, JJ3T, IPH;
   int INDA, INDB, INDC, INDMAX;
   int IS, J12T, J12TD, J23T, J23TS, J2S, J2SB;
   int J3S, J3SB, J3SQ, J3T, J3TD, JJ12T, JJ12TA;
   int JJ12TB, JTDMAX, JTDMIN;
   int IE3MAX, IES, IESJ3S, IESMAX;
   int NA, NAB, NABC, NNCD, IDQ, JD, KD, JDKD, KDID;
   int KA, KABC, KABCD, KAIA;
   int KB, KBCDQ, KBCQ, KBIB, KBQ, KC, KCDQ, KCIC;
   int KCQ, KDQ, NECA, NECB, NECC, NECD;

   int X13 = lm23 + mu23 + 1;
   int X14 = X13 * rho1_23max;
   int X17 = lm1 + mu1 + 1;

   std::vector<int> IA(X13, 0);

   int n2 = (lm3 + mu3 + 1);
   std::vector<int> INDMAT, J2SMAX, J2TMAX;
   std::vector<int> J3SMAX, J3TMAX;

   std::vector<int> JXTA, JYTA, IEA;
   std::vector<int> JXTB, JYTB, IEB;
   std::vector<int> JXTC, JYTC, IEC;
   std::vector<int> JXTD, JYTD, IED;
   std::vector<int> J1SMAX, J1TMAX;

   std::vector<double> DA(X14, 0.0);
   std::vector<double> DB(rho1_23max, 0), DT(rho1_23max, 0);

   double D1, D2, DC;

   NA = rho12max;
   NAB = NA * rho12_3max;
   NABC = NAB * rho23max;
   std::vector<double> DEWU3D;
   xewu3(lm23, mu23, lm1, mu1, lm, mu, 1, NECD, rho1_23max, JXTD, JYTD, IED, DEWU3D);
   INDMAX = JXTD.size();
   NNCD = NECD + 1;
   IDQ = (INDMAX - NNCD - 1) * rho1_23max;
   for (JD = 1; JD <= NNCD; ++JD)  
   {
      IDQ = IDQ + rho1_23max;
      IA[JD - 1] = JD;
      KDQ = -X13;
      for (KD = 1; KD <= rho1_23max; ++KD) 
      {
         KDQ = KDQ + X13;
         JDKD = JD + KDQ;
         KDID = KD + IDQ;
         DA[JDKD - 1] = DEWU3D[KDID - 1];
      }
   }
   dlut(NNCD, rho1_23max, IA, DA, X13);
   // for large value of rho12max, we need to use higher precision to
   // compute SU(3)>SU(2)xU(1) coefficients
   // (lm2 mu2) x (lm1 mu1) -> rho12max(lm12 mu12)
   std::vector<T> DEWU3A;
   xewu3_internal(lm2, mu2, lm1, mu1, lm12, mu12, 1, NECA, rho12max, JXTA, JYTA, IEA, DEWU3A);
   std::vector<double> DEWU3C;
   xewu3(lm2, mu2, lm3, mu3, lm23, mu23, 1, NECC, rho23max, JXTC, JYTC, IEC, DEWU3C);
   std::vector<double> DEWU3B;
   xewu3(lm12, mu12, lm3, mu3, lm, mu, 1, NECB, rho12_3max, JXTB, JYTB, IEB, DEWU3B);
   I1 = lm + 2 * mu;
   I2 = lm12 + 2 * mu12;
   I3 = 4 * lm12 + 2 * mu12;
   I4 = 2 * I2;
   I5 = 2 * (lm12 - mu12);
   I6 = lm3 + 2 * mu3;
   I7 = 2 * (lm3 - mu3);
   IE1 = lm23 + 2 * mu23 - I1;
   J1TS = lm1 + NECD + 2;
   // higher precision needed to accuractely compute CGs for large values of rho12max.
   std::vector<T> DWU3;
   J3TDMA = std::min(NECB, NECC) + 1;
   for (J3S = 1; J3S <= J3TDMA; ++J3S) 
   {
      J3TD = J3S - 1;
      IE3 = -I6 + 3 * J3TD;
      JJ3TA = lm3 + J3TD;
      IS = I6 - J3TD;
      if (IS < JJ3TA) {
         JJ3TA = IS;
      }
      JJ3TA = JJ3TA + 1;
      IS = (I7 - IE3) / 3;
      JJ3TB = JJ3TA - std::abs(IS);
      J12TD = NECB - J3TD;
      J2TD = NECC - J3TD;
      IE12 = -I2 + 3 * J12TD;
      JJ12TA = I3 + IE12;
      IS = I4 - IE12;
      if (IS < JJ12TA) {
         JJ12TA = IS;
      }
      JJ12TA = JJ12TA / 3 + 1;
      IS = (I5 - IE12) / 3;
      JJ12TB = JJ12TA - std::abs(IS);
      for (JJ12T = 1; JJ12T <= JJ12TB; JJ12T += 2)  // DO 35 JJ12T=1,JJ12TB,2
      {
         J12T = JJ12TA - JJ12T;
         // for large value of rho12max, we need to use higher precision to
         // compute SU(3)>SU(2)xU(1) coefficients accurately
         xwu3_internal(lm2, mu2, lm1, mu1, lm12, mu12, IE12, J12T, DEWU3A, rho12max, INDMAX, J2SMAX,
                       J2TMAX, J1SMAX, J1TMAX, IE1MAX, INDMAT, DWU3);
         IESMAX = J1TMAX.size();
         IE1TES = IE1MAX - IE1;
         if (IE1TES < 0) {
            continue;
         }
         if (IE1TES > 3 * (IESMAX - 1)) {
            continue;
         }
         IES = (IE1 - IE1MAX + 3 * IESMAX) / 3;
         KDQ = -NABC;
         for (KD = 1; KD <= rho1_23max; ++KD) 
         {
            KDQ = KDQ + NABC;
            J1T = J1TS - 2 * IA[KD - 1];
            J1S = (J1TMAX[IES - 1] + 2 - J1T) / 2;
            if (J1S < 1) {
               continue;
            }
            if (J1S > J1SMAX[IES - 1]) {
               continue;
            }
            J1SQ = X17 * (J1S - 1);
            IESJ1S = IES + J1SQ;
            J2T = J2TMAX[IESJ1S - 1] + 2;
            INDA = (INDMAT[IESJ1S - 1] - J2T) / 2;
            J2SB = J2SMAX[IESJ1S - 1];
            for (J2S = 1; J2S <= J2SB; ++J2S)
            {
               J2T = J2T - 2;
               IAQ = INDA * rho12max;
               INDA = INDA + 1;
               for (JJ3T = 1; JJ3T <= JJ3TB; JJ3T += 2) 
               {
                  J3T = JJ3TA - JJ3T;
                  INDC = INDEX(J2TD, lm2, J2T, J3TD, lm3, J3T) + 1;
                  if (JXTC[INDC - 1] < 0) {
                     continue;
                  }
                  INDB = INDEX(J12TD, lm12, J12T, J3TD, lm3, J3T) + 1;
                  if (JXTB[INDB - 1] < 0) {
                     continue;
                  }
                  IPH = J2T + lm + 3 * (J12T + lm23);
                  D2 = (double)(1 - std::abs(IPH) % 4);
                  DC = D2 * drr3(J1T, J2T, lm, J3T, J12T, lm23);
                  DC = DC * std::sqrt((double)((J12T + 1) * (lm23 + 1)));
                  ICQ = (INDC - 1) * rho23max;
                  IBQ = (INDB - 1) * rho12_3max;
                  KCQ = -NAB;
                  for (KC = 1; KC <= rho23max; ++KC)  
                  {
                     KCQ = KCQ + NAB;
                     KCDQ = KCQ + KDQ;
                     KCIC = KC + ICQ;
                     KBQ = -NA;
                     for (KB = 1; KB <= rho12_3max; ++KB) 
                     {
                        KBQ = KBQ + NA;
                        KBCDQ = KBQ + KCDQ;
                        KBIB = KB + IBQ;
                        for (KA = 1; KA <= rho12max; ++KA) 
                        {
                           KABCD = KA + KBCDQ;
                           KAIA = KA + IAQ;
                           dzu3[KABCD - 1] = dzu3[KABCD - 1] + DC * DEWU3C[KCIC - 1] *
                                                                   DEWU3B[KBIB - 1] *
                                                                   (double)DWU3[KAIA - 1];
                        }
                     }
                  }
               }
            }
         }
      }
   }

   KCQ = -NAB;
   for (KC = 1; KC <= rho23max; ++KC)  
   {
      KCQ = KCQ + NAB;
      KBQ = -NA;
      for (KB = 1; KB <= rho12_3max; ++KB)  
      {
         KBQ = KBQ + NA;
         KBCQ = KBQ + KCQ;
         for (KA = 1; KA <= rho12max; ++KA)  
         {
            KABC = KA + KBCQ;
            KDQ = -NABC;
            for (KD = 1; KD <= rho1_23max; ++KD)  
            {
               KDQ = KDQ + NABC;
               KABCD = KABC + KDQ;
               DB[KD - 1] = dzu3[KABCD - 1];
            }
            if (rho1_23max > 1) {
               dbsr(rho1_23max, DA, DB, DT, X13);
            } else {
               DB[0] = DB[0] / DA[0];
            }
            KDQ = -NABC;
            for (KD = 1; KD <= rho1_23max; ++KD) 
            {
               KDQ = KDQ + NABC;
               KABCD = KABC + KDQ;
               dzu3[KABCD - 1] = DB[KD - 1];
            }
         }
      }
   }
}
} // namespace su3

#endif
