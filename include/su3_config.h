#ifndef SU3_SU3_CONFIG_H
#define SU3_SU3_CONFIG_H

// uncomment (or define outside) if long double is implemented 
// as 80-bit floaging point type by hardware
//#define HAVE_80BIT_LONG_DOUBLE

// uncomment (or define outside) for using float128 
//#define HAVE_FLOAT128

// uncomment for dtu3r3 caching:
//#define DTU3R3_CACHE

namespace su3
{
   // allow (lm mu) with lm + mu <= max_lmmu
   // set in su3::init/su3::init_thread
   extern int max_lmmu;
}

#endif
