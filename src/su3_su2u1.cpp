#include <algorithm>
#include <cassert>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <tuple>
#include <vector>

#include <su3.h>

#include <boost/multiprecision/cpp_bin_float.hpp>
#include <boost/multiprecision/float128.hpp>

namespace su3 {

int multhy(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3) {
   int ixmin = std::numeric_limits<int>::max();
   int ix[6];
   ix[0] = lm1 + lm2 - lm3 + 2 * (mu1 + mu2 - mu3);
   ix[1] = mu1 + mu2 - mu3 + 2 * (lm1 + lm2 - lm3);
   ix[2] = 2 * lm2 + mu2 + mu1 - lm1 - mu3 + lm3;
   ix[3] = 2 * mu2 + lm2 + lm1 - mu1 - lm3 + mu3;
   ix[4] = lm3 + mu2 - lm1 + 2 * (mu3 + lm2 - mu1);
   ix[5] = mu3 + lm2 - mu1 + 2 * (lm3 + mu2 - lm1);

   for (size_t i = 0; i < 6; ++i) {
      if (ix[i] % 3) {
         return 0;
      }
      int ixdb3 = ix[i] / 3;
      if (ixdb3 < ixmin) {
         ixmin = ixdb3;
      }
   }
   if (ixmin < 0) {
      return 0;
   }
   return std::min({ixmin, lm2, mu2}) + 1;
}

Generator_LM1LM2eps2toHW::Generator_LM1LM2eps2toHW(int lm1, int mu1, int lm3, int mu3) {
   lm3_ = lm3;
   ie3_ = -(lm3 + 2 * mu3);

   lm1_ = lm1;
   ia1_ = 2 * lm1 + 4 * mu1;
   ib1_ = 4 * lm1 + 2 * mu1;
   ic1_ = ib1_ - ia1_;
}

int Generator_LM1LM2eps2toHW::generate(int lm2, int mu2, int nnc, std::vector<int>& j1ta,
                                       std::vector<int>& j2ta, std::vector<int>& ie2a) {
   int ia2 = 2 * lm2 + 4 * mu2;
   int ib2 = 4 * lm2 + 2 * mu2;
   int ic2 = ib2 - ia2;  // 2*lm2 - 2*mu2 = 2(lm2 - mu2)
   int ie2 = -(lm2 + 2 * mu2);

   int I = 1000;
   // iterate over all possible values of eps2 that can couple with some eps1 to eps_3HW.
   for (int iie = 1; iie <= nnc; ++iie, ie2 += 3) {
      int ie1 = ie3_ - ie2;
      int j2td = iie - 1;
      int j1td = nnc - iie;

      int jj2ta = ia2 - ie2;
      int jj2tb = ib2 + ie2;
      if (jj2tb < jj2ta) {
         jj2ta = jj2tb;
      }
      jj2ta = jj2ta / 3 + 1;
      jj2tb = jj2ta - std::abs(ic2 - ie2) / 3;
      int jj1ta = ia1_ - ie1;
      int jj1tb = ib1_ + ie1;
      if (jj1tb < jj1ta) {
         jj1ta = jj1tb;
      }
      jj1ta = jj1ta / 3 + 1;
      jj1tb = jj1ta - std::abs(ic1_ - ie1) / 3;
      int J = 0;
      for (int jj2t = 1; jj2t <= jj2tb; jj2t += 2) {
         int j2t = jj2ta - jj2t;
         int L = std::abs(j2t - lm3_);
         int M = j2t + lm3_;
         for (int jj1t = 1; jj1t <= jj1tb; jj1t += 2) {
            int j1t = jj1ta - jj1t;
            if (j1t < L) {
               continue;
            }
            if (j1t > M) {
               continue;
            }
            size_t ind = su3::INDEX(j1td, lm1_, j1t, j2td, lm2, j2t);
            //               std::cout << "ind:" << ind << " e1:" << ie1 << " j1t:" << j1t
            //                         << " e2 : " << ie2 << " j2t:" << j2t << std::endl;
            j1ta[ind] = j1t;
            j2ta[ind] = j2t;
            ie2a[ind] = ie2;
            J = J + 1;
         }  // 50
      }     // 50
      if (J < I) {
         I = J;
      }
   }  // 55
   return I;
}

static void ShowRangeErrorXEWU3Terminate(int lm, int mu) {
   std::cerr << "Error in input parameters of xewu3 [SU(3)>SU(3)xU(1) extreme coeffs]: (" << lm
             << " " << mu << ") is beyond the current limit (lm + mu) <= " << su3::max_lmmu
             << std::endl;
   exit(EXIT_FAILURE);
}

// Computes extreme CGs with appropriate accuracy set according to rhomax.
// results returnes as vector<double>
void xewu3(int lm1x, int mu1x, int lm2x, int mu2x, int lm3x, int mu3x, int I3, int& nec, int rhomax,
           std::vector<int>& j1ta, std::vector<int>& j2ta, std::vector<int>& iea2,
           std::vector<double>& su3cgs_results) 
{
   assert(rhomax <= 35);

   if (lm1x + mu1x > su3::max_lmmu) {
      ShowRangeErrorXEWU3Terminate(lm1x, mu1x);
   }
   if (lm1x + mu1x > su3::max_lmmu) {
      ShowRangeErrorXEWU3Terminate(lm2x, mu2x);
   }
   if (lm3x + mu3x > su3::max_lmmu) {
      ShowRangeErrorXEWU3Terminate(lm3x, mu3x);
   }

   if (rhomax <= 12) { // rhomax=12 max|Oij|=1.1e-14
      xewu3_internal<double>(lm1x, mu1x, lm2x, mu2x, lm3x, mu3x, I3, nec, rhomax, j1ta, j2ta, iea2,
                             su3cgs_results); 
   } 
   else if (rhomax <= 19) { // rhomax=19 => max|Oij|=1.1e-14
#ifdef HAVE_80BIT_LONG_DOUBLE
      xewu3_internal_to_dbl<long double>(lm1x, mu1x, lm2x, mu2x, lm3x, mu3x, I3, nec, rhomax, j1ta, j2ta,
                                  iea2, su3cgs_results);
#else
      xewu3_internal_to_dbl<boost::multiprecision::cpp_bin_float_double_extended>(
          lm1x, mu1x, lm2x, mu2x, lm3x, mu3x, I3, nec, rhomax, j1ta, j2ta, iea2, su3cgs_results);
#endif
   } else { // rhomax=34 => max|O_{ij}|=1e-14
#ifdef HAVE_FLOAT128
      xewu3_internal_to_dbl<boost::multiprecision::float128>(lm1x, mu1x, lm2x, mu2x, lm3x, mu3x, I3, nec,
                                                      rhomax, j1ta, j2ta, iea2, su3cgs_results);
#else
      xewu3_internal_to_dbl<boost::multiprecision::cpp_bin_float_quad>(
          lm1x, mu1x, lm2x, mu2x, lm3x, mu3x, I3, nec, rhomax, j1ta, j2ta, iea2, su3cgs_results);
#endif
   }
}

// Simplified interface to xwu3.
// Accuracy is set according to rhomax.
// Extreme SU(3)>SU(2)xU(1) coefficients are computed each time xwu3 is executed.
// For large-scale computations may be advisable to store extreme coefficients in fast look-up table
//
// std::vector<std::tuple<int, int, int>> = vector<e2, 2*LM2, 2*LM1>
void xwu3(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3, int eps3, int LM3, int rhomax,
          std::vector<std::tuple<int, int, int>>& labels_e2LM2LM1, std::vector<double>& dwu3) {
   assert(rhomax <= 35);

   if (lm1 + mu1 > su3::max_lmmu) {
      ShowRangeErrorXEWU3Terminate(lm1, mu1);
   }
   if (lm2 + mu2 > su3::max_lmmu) {
      ShowRangeErrorXEWU3Terminate(lm2, mu2);
   }
   if (lm3 + mu3 > su3::max_lmmu) {
      ShowRangeErrorXEWU3Terminate(lm3, mu3);
   }

   if (rhomax <= 12) // rhomax=12 => max|Oij| = 1e-14
   {
      xwu3_helper<double>(lm1, mu1, lm2, mu2, lm3, mu3, eps3, LM3, rhomax, labels_e2LM2LM1, dwu3);
   }
   else if (rhomax <= 15) // rhomax=12 => max|Oij| = 1e-15
   {
#ifdef HAVE_80BIT_LONG_DOUBLE
      xwu3_helper<long double>(lm1, mu1, lm2, mu2, lm3, mu3, eps3, LM3, rhomax, labels_e2LM2LM1,
                               dwu3);
#else
      xwu3_helper<boost::multiprecision::cpp_bin_float_double_extended>(
          lm1, mu1, lm2, mu2, lm3, mu3, eps3, LM3, rhomax, labels_e2LM2LM1, dwu3);
#endif
   }
   else if (rhomax <= 27) // rhomax=17 => max|Oij| = 1e-15
   {
#ifdef HAVE_FLOAT128
      xwu3_helper<boost::multiprecision::float128>(lm1, mu1, lm2, mu2, lm3, mu3, eps3, LM3, rhomax,
                                                    labels_e2LM2LM1, dwu3);
#else
      xwu3_helper<boost::multiprecision::cpp_bin_float_quad>(lm1, mu1, lm2, mu2, lm3, mu3, eps3,
                                                              LM3, rhomax, labels_e2LM2LM1, dwu3);
#endif      
   }
   else
   {
      xwu3_helper<boost::multiprecision::cpp_bin_float_oct>(lm1, mu1, lm2, mu2, lm3, mu3, eps3,
                                                             LM3, rhomax, labels_e2LM2LM1, dwu3);
   }
}
}  // namespace su3
