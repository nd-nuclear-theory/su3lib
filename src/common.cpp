#include <boost/multiprecision/cpp_bin_float.hpp>
#include <boost/multiprecision/float128.hpp>

extern "C"
{
#include <wigxjpf.h>
}

#include <su3.h>

namespace su3
{

// explicit instantiation:
template class bc<double>;
#ifdef HAVE_80BIT_LONG_DOUBLE
template class bc<long double>;
#else
template class bc<boost::multiprecision::cpp_bin_float_double_extended>;
#endif
#ifdef HAVE_FLOAT128
template class bc<boost::multiprecision::float128>;
#else
template class bc<boost::multiprecision::cpp_bin_float_quad>;
#endif
template class bc<boost::multiprecision::cpp_bin_float_oct>;

void internal_init()
{
   // max_lmmuL: max(lm + mu + L)
   // max(L) = max(lm + mu) => max(lm + mu + L) = 2*max(lm + mu)
   const size_t max_lmmuL = 2*su3::max_lmmu;
   // Following variables set the size of arrays with binomial
   // coefficients ...
   //
   // although double can hold (n,k) up to n=56 without loss of precision
   // F77 su3lib uses double to store (n,k) up to n=128.
   const size_t nmax_double = 56;

   // quad can hold (n,k) up to n=117 without loss of precision
   // F77 su3lib uses quad precison to store (n,k) up to 192
   const size_t nmax_quad = 117;

   // oct can hold (n,k) up to n=241 without loss of precision. Current limit 2*(lm_max + mu_max) <=
   // 512 means one can compute, e.g., (128 128) Lmax=256
   const size_t nmax_oct = 512;

   bc<double>::init(std::min(nmax_double, max_lmmuL));

   // bino<long double> is also used for computation of SU(3) > SU(2)xU(1) coeffs
   // where largest binomial coefficient needed is (rhomax, *).
   // Thus we limit its range by max(lm + mu + L) to be able to handle
   // rhomax <= max(lm + mu + L)
#ifdef HAVE_80BIT_LONG_DOUBLE
   bc<long double>::init(max_lmmuL);
#else
   bc<boost::multiprecision::cpp_bin_float_double_extended>::init(max_lmmuL);
#endif
#ifdef HAVE_FLOAT128
   bc<boost::multiprecision::float128>::init(std::min(nmax_quad, max_lmmuL));
#else
   bc<boost::multiprecision::cpp_bin_float_quad>::init(std::min(nmax_quad, max_lmmuL));
#endif
   bc<boost::multiprecision::cpp_bin_float_oct>::init(std::min(nmax_oct, max_lmmuL));
}

} // namespace su3
