extern "C" {
#include <wigxjpf.h>
}

#include <su3.h>

namespace su3 {
// declared in su3/config.h
int max_lmmu;

// maximal value (lm + mu) is implicitly set to 100
// jjmax implicitly set to -1, in which case 2JJmax = max(lm + mu)
void init(int max_lm_p_mu, int jjmax) {
   // internal_init() uses su3::max_lmmu => this needs this to be set here
   su3::max_lmmu = max_lm_p_mu;
   // max2LM = 2*max(LM) = max(lm + mu);
   const int max2LM = su3::max_lmmu;
   // su3lib needs 3j and 6j symbols only. However, we expect user to need also 9j symbols as well.
   // if jjmax=max(2j)=-1 => set max(2j)=2max(lm + mu) to allow J values up to |L+S|<=2max(lm+mu)
   if (jjmax == -1)
   {
      wig_table_init(2 * max2LM, 9);
      wig_temp_init(2 * max2LM);
   }
   else
   {
      wig_table_init(jjmax, 9);
      wig_temp_init(jjmax);
   }

   internal_init();

#ifdef DTU3R3_CACHE
   dtu3r3_cache_init();
#endif
}

// maximal value (lm + mu) is implicitly set to 100
// jjmax implicitly set to -1, in which case 2JJmax = max(lm + mu)
void init_thread(int max_lm_p_mu, int jjmax) {
   // internal_init() needs this to be set
   su3::max_lmmu = max_lm_p_mu;
   // max2LM = 2*max(LM) = max(lm + mu);
   const int max2LM = su3::max_lmmu;

   if (jjmax == -1) {
// must be called by a single thread only
#pragma omp single
      wig_table_init(2 * max2LM, 9);

      wig_thread_temp_init(2 * max2LM);
   } else {
#pragma omp single
      wig_table_init(jjmax, 9);

      wig_thread_temp_init(jjmax);
   }

#pragma omp single
   internal_init();

#ifdef DTU3R3_CACHE
   dtu3r3_cache_init();
#endif
}

void finalize() {
   wig_temp_free();
   wig_table_free();

#ifdef DTU3R3_CACHE
   dtu3r3_cache_finalize();
#endif
}

void finalize_thread() {
   wig_temp_free();

// must be called by a single thread only
#pragma omp single
   wig_table_free();

#ifdef DTU3R3_CACHE
   dtu3r3_cache_finalize();
#endif
}

}  // namespace su3
